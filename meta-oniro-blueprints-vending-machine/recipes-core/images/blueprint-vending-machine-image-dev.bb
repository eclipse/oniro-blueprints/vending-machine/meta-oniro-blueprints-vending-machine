# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

HOMEPAGE = "https://docs.oniroproject.org/"
SUMMARY = "Vending machine blueprint image"
DESCRIPTION = "The Vending Machine image is composed of a couple of applications"
LICENSE = "Apache-2.0"

require recipes-core/images/oniro-image-base.bb

REQUIRED_DISTRO_FEATURES = "\
	wayland \
	debug-tweaks \
	tools-debug \
"

IMAGE_INSTALL:append = "\
	vending-machine-control-application \
	vending-machine-control-application-dbg \
	vending-machine-control-application-src \
	vending-machine-ui-application \
	vending-machine-ui-application-dbg \
	vending-machine-ui-application-src \
	lvgl-dbg \
	lvgl-src \
	weston \
	weston-dbg \
	weston-src \
	weston-init \
	"
