# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

EXTRA_OEMESON += "-Ddeprecated-wl-shell=true"
